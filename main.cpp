#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

#include <random>
#include <chrono>

using namespace std;

template <class T>
void debugContent(const T &container)
{
    for (auto e : container)
    {
        cout << e << ' ';
    }
    cout << endl;
}

vector<int> randomVector(int size = 10, int low = -1000000, int high = 1000000)
{
    auto seed = std::chrono::system_clock::now().time_since_epoch().count(); //seed
    std::default_random_engine dre(seed);                                    //engine
    std::uniform_int_distribution<int> di(low, high);                        //distribution

    std::vector<int> v(size);
    std::generate(v.begin(), v.end(), [&] { return di(dre); });
    return v;
}

// https://app.codility.com/programmers/lessons/4-counting_elements/missing_integer/
int solution1(vector<int> &A)
{
    vector<int> P;
    P.reserve(A.size());
    for (auto e : A)
        if (e >= 0)
            P.push_back(e);
    sort(P.begin(), P.end());
    for (int i = 0; i <= P.size() - 1; ++i)
    {
        int d = P[i + 1] - P[i];
        if (d == 0)
            continue;
        if (d == 1)
            continue;
        return P[i] + 1;
    }
    return 1;
}

// https://app.codility.com/programmers/lessons/4-counting_elements/max_counters/
vector<int>
solution2(int N, vector<int> &A)
{
    vector<int> C;
    C.assign(N, 0);
    for (auto e : A)
    {
        int pos = e - 1;
        if (pos >= N)
        {
            //max
            int m = *max_element(C.begin(), C.end());
            C.assign(N, m);
        }
        else
        {
            ++C[pos];
        }
    }
    return C;
}

// https://app.codility.com/programmers/lessons/6-sorting/number_of_disc_intersections/
int solution3(vector<int> &A)
{
    int n = 0;
    for (int i = 0; i < A.size() - 1; ++i)
    {
        for (int j = i + 1; j < A.size(); ++j)
        {
            if (j - i <= A[i] + A[j])
            {
                ++n;
            }
        }
    }
    return n;
}

int main()
{
    cout << "@ 1 @" << endl;
    {
        vector<int> A = {1, 3, 6, 4, 1, 2};
        debugContent(A);
        cout << solution1(A) << endl;

        vector<int> B = {1, 3, 6, 2, 1, 4};
        debugContent(B);
        cout << solution1(B) << endl;

        vector<int> R = randomVector(10);
        debugContent(R);
        cout << solution1(R) << endl;
    }

    //##################

    cout << "@ 2 @" << endl;

    {
        vector<int> A = {3, 4, 4, 6, 1, 4, 4};
        debugContent(A);
        vector<int> S = solution2(5, A);
        debugContent(S);
    }

    //##################

    cout << "@ 3 @" << endl;

    {
        vector<int> A = {1, 5, 2, 1, 4, 0};
        debugContent(A);
        int S = solution3(A);
        cout << S << endl;
    }

    return 0;
}
